<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/tom-select@2.3.1/dist/css/tom-select.bootstrap5.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/tom-select@2.3.1/dist/js/tom-select.complete.min.js"></script>
    <script src="https://unpkg.com/htmx.org@1.9.8"></script>
    <title>@yield('title')|Admin</title>
    <style>
      @layer reset{
        button{
          all:unset;
        }
      }

      .htmx-indicator{
        display:none;
      }
      .htmx-request .htmx-indicator{
        display: inline-block;
      }
      .htmx-request.htmx-indicator{
        display: inline-block;
      }
    </style>
</head>
<body>

    {{-- Start navbar --}}
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="/">Agence</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          @php
              $route = request()->route()->getName();
          @endphp
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link" aria-current="page" href="property">Home</a>
              </li>
              <li class="nav-item">
                <a @class(['nav-link', 'active' => str_contains($route, 'property.')]) href="{{ route('admin.property.index')}}">Gérer les biens</a>
              </li>
              <li class="nav-item">
                <a @class(['nav-link', 'active' => str_contains($route, 'option.')]) href="{{ route('admin.option.index')}}">Gérer les options</a>
              </li>
              {{--<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
               <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form> --}}


          </div>

          {{-- Deconnection --}}
          <div class="ms-auto">
            @auth
              <ul class="navbar-nav">
                <li class="nav-items">

                  <form action="{{ route('logout') }}" method="post">
                  @csrf
                  @method('delete')
                  <button class="nav-link">Se déconnecter</button>
                  </form>

                </li>
              </ul>
            @endauth
          </div>
        </div>
    </nav>
      {{-- end navbar --}}
      

    <div class="container mt-5">
        @include('shared.flash')

        @yield('content')
    </div>

    <script>
      new TomSelect('select[multiple]', {plugins: {remove_button: {title: 'Supprimer'}}})
    </script>
</body>
</html>