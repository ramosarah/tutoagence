@extends('base')

@section('title', 'Tous nos biens')

@section('content')
    <div class="bg-light p-5 mb-5 text-center">
        <form action="" method="get" class="container d-flex gap-2">
            <input type="number" class="form-control" name="surface" value="{{ $input['surface'] ?? '' }}" placeholder="Surface min">
            <input type="number" class="form-control" name="rooms" value="{{ $input['rooms'] ?? '' }}" placeholder="Nombre de pièces min">
            <input type="number" class="form-control" name="price" value="{{ $input['price'] ?? '' }}" placeholder="Budget Max">
            <input type="text" class="form-control" name="title" value="{{ $input['title'] ?? '' }}" placeholder="Mots clés">
            <button class="btn btn-primary btn-sm flex-grow-0">
                Recherche
            </button>
        </form>
    </div>

    <div class="container">
        <div class="row">
            @forelse ($properties as $property)
            <div class="col-3 mb-4">
                @include('shared.card')
            </div>
            @empty
            <div class="col-5 mb-4">
                Aucun bien ne correspond à votre recherche
            </div>
            @endforelse
        </div>

        <div class="my-4">
            {{ $properties->links() }}
        </div>
    </div>

@endsection