<x-mail::message>
# Nouvelle demande de coontact

Nouvelle demande pour le bien <a href="{{ route('property.show', ['slug' => $property->getSlug(), 'property' => $property]) }}" target="_blank">
{{ $property->title }}
</a>.

- Prénom: {{ $data['firstname'] }}
- Nom: {{ $data['lastname'] }}
- Tél.: {{ $data['phone'] }}
- E-mail: {{ $data['email'] }}

**Message** <br/>
{{ $data['message'] }}

</x-mail::message>
