<div class="card">
    @if ($property->getPicture())
        <img src="{{ $property->getPicture()->getImageUrl(360, 230) }}" class="w-25" alt="Image d'illustration">
    @else
        <img src="/empty.jpg" class="w-25" alt="Image d'illustration">
    @endif
    <div class="car-body">
        <h5 class="card-title">
            <a href="{{ route('property.show', ['slug' => $property->getSlug(), 'property' => $property]) }}">{{ $property->title}}</a>
        </h5>
        <div class="text-primary" style="font-size: 1.4rem;">
            {{ number_format($property->price, thousands_separator: ' ')}} CHF
        </div>
    </div>
</div>