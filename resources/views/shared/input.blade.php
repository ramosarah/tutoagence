@php
$label ??= null; // ??= si il n'y a pas $ alors met null
$type ??= 'text';
$class ??= null;
$name ??= '';
$value ??= '';
@endphp

<div @class(['form-group', $class])>
    <label for="{{ $name }}">{{ $label }}</label>
    @if ($type == 'textarea')
        <textarea class="form-control @error($name) is-invalid @enderror" type="{{ $type }}" id="{{ $name }}" name="{{ $name }}">{{ old($name, $value) }}</textarea>
        
    @else
        <input class="form-control @error($name) is-invalid @enderror" value="{{ old($name, $value) }}" type="{{ $type }}" id="{{ $name }}" name="{{ $name }}">
        
    @endif

    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>