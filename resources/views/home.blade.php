@extends('base')


@section('content')


<div class="container">
<x-weather></x-weather>    
<div class="row">
        <p class="text-start">Start aligned text on all viewport sizes.</p>
        <p class="text-center">Center aligned text on all viewport sizes.</p>
        <p class="text-end">End aligned text on all viewport sizes.</p>
        <p class="text-sm-start">Start aligned text on viewports sized SM (small) or wider.</p>
        <p class="text-md-start">Start aligned text on viewports sized MD (medium) or wider.</p>
        <p class="text-lg-start">Start aligned text on viewports sized LG (large) or wider.</p>
        <p class="text-xl-start">Start aligned text on viewports sized XL (extra-large) or wider.</p>
    </div>
</div>

<div class="container mt-5 pb-5 bg-secondary text-white">
    <h2>Nos biens du moment</h2>
    <div class="row">
        @foreach ( $properties as $property)
            <div class="col">
                @include('shared.card')
            </div>
        @endforeach
    </div>
</div>



@endsection