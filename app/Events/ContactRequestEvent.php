<?php

namespace App\Events;

use App\Models\Property;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ContactRequestEvent
{
    use Dispatchable;

    /**
     * Create a new event instance.
     */
    public function __construct(public Property $property, public array $data)
    {
    }

}
