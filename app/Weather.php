<?php 

namespace App;
use Illuminate\Support\Facades\Cache;

class Weather 
{
    //on imagine qu'on doit taper dans une API
    /* public function __construct(public string $apiKey) {

    } */


    public function isSunnyTomorrow(): bool {

        $result = Cache::get('weather');
        
        if($result !== null) {
            return $result;
        }
        
        return true;
    }
}