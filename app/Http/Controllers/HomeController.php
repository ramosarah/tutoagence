<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\User;
use App\Weather;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function  index(Weather $weather) {
        //dd($weather);
        //dd(app(Weather::class)); Si pas déclarer dans les paramètres de la fonction
        $properties = Property::with('pictures')->available()->recent()->limit(4)->get(); //pour available re recent voir méthodes dans Propoerty Model
        //$user = User::first();
        return view('home', ['properties' => $properties]);
    }
}
