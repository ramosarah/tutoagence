<?php

use App\Http\Controllers\Admin\OptionController;
use App\Http\Controllers\Admin\PictureController;
use App\Http\Controllers\Admin\PropertyController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ViewPropertyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
$idRegex = '[0-9]+';
$slugRegex = '[0-9a-z\-]+';
Route::get('/', [HomeController::class, 'index']);
Route::get('/biens', [ViewPropertyController::class, 'index'])->name('property.index');
Route::get('/biens/{slug}-{property}', [ViewPropertyController::class, 'show'])->name('property.show')->where([
    'property' => $idRegex,
    'slug' => $slugRegex,
]);

Route::post('/biens/{property}/contact', [ViewPropertyController::class, 'contact'])->name('property.contact')->where([
    'property' => $idRegex,
]);;

Route::post('/login', [AuthController::class, 'doLogin']);
Route::get('/login', [AuthController::class, 'login'])
    ->middleware('guest')
    ->name('login');
Route::delete('/logout', [AuthController::class, 'logout'])
    ->middleware('auth') //on peut pas se déconnecter si on est pas connecté
    ->name('logout');


Route::prefix('admin')->name('admin.')->middleware('auth')->group( function() use ($idRegex) {
    Route::resource('property', PropertyController::class)->except(['show']);
    Route::resource('option', OptionController::class)->except(['show']);
    Route::delete('picture/{picture}', [PictureController::class, 'destroy'])->name('picture.destroy')
    ->where(['picture' => $idRegex])
    ->can('delete', 'picture');
});

Route::get('/images/{path}', [PictureController::class, 'show'])->where('path', '.*');
