<?php

namespace Tests\Unit;

use App\Weather;
use Illuminate\Support\Facades\Cache;
use PHPUnit\Framework\TestCase;

class WeatherTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function test_example(): void
    {
        Cache::shouldReceive('get')->with('weather')->once()->andReturn(null);
        $weather = new Weather();
        $this->assertTrue($weather->isSunnyTomorrow());
    }


    public function test_example_false(): void
    {
        Cache::shouldReceive('get')->with('weather')->once()->andReturn(false);
        $weather = new Weather();
        $this->assertFalse($weather->isSunnyTomorrow());
    }
}
