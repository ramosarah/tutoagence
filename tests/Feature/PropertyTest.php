<?php

namespace Tests\Feature;

use App\Models\Property;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_send_not_found_on_non_existent_property(): void
    {
        $response = $this->get('/biens/unde-vel-dolore-qui-atque-incidunt-rerum-3');

        $response->assertStatus(404);
    }

    public function test_ok_on_bad_slug_property(): void
    {
        /** @var Property $property */

        $property = Property::factory()->create();
        $response = $this->get('/biens/unde-vel-dolore-qui-atque-incidunt-rerum-' . $property->id);

        $response->assertRedirectToRoute('property.show', ['property' => $property->id, 'slug' => $property->getSlug()]);
    }


    public function test_ok_on_property(): void
    {
        /** @var Property $property */

        $property = Property::factory()->create();
        $response = $this->get("/biens/{$property->getSlug()}-{$property->id}"); // /!\double quotes importante
        $response->assertOk();
        $response->assertSee($property->title);
    }


    public function test_error_on_contact():void {
        /** @var Property $property */

        $property = Property::factory()->create();
        $response = $this->post("/biens/{$property->id}/contact", [
            'firstname' => 'John',
            'lastname' => 'doe',
            'phone' => '000000000000000000',
            'email' => 'john',
            'message' => 'contact me please',
        ]);
        $response->assertRedirect();
        $response->assertSessionHasErrors(['email']); //je m'attend à ce qu'il y ait des erreur dans email
        $response->assertSessionHasInput('email', 'john');
        //$response->assertSessionHasInput('email', 'doe');

    }

    public function test_ok_on_contact():void {
        Notification::fake();
        /** @var Property $property */

        $property = Property::factory()->create();
        $response = $this->post("/biens/{$property->id}/contact", [
            'firstname' => 'John',
            'lastname' => 'doe',
            'phone' => '000000000000000000',
            'email' => 'john@doe.fr',
            'message' => 'contact me please',
        ]);
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('success');
        //Notification::assertSentOnDemand(1);

    }
}
